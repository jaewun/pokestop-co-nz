{{-- $category is passed as NULL to the master layout view to prevent it from showing in the breadcrumbs --}}
@extends ('forum::master', ['category' => null])

@section ('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-info">
                <h4>Welcome to PokéStop.co.nz</h4>
                <p>
                    PokéStop is an online community for Pokémon Trainers in New Zealand. We're non-profit, and all
                    about being the very best - like no one ever was. We're building PokéStop from the ground
                    up, so it can be the perfect stop for New Zealand Pokémon Trainers.
                <p>
                    In the near future we hope to add great futures such as...
                </p>
                <p>
                <ul>
                    <li>Your Pokédex</li>
                    <li>Gym Tracking</li>
                    <li>Team Leader Boards</li>
                    <li>Private Team Forums</li>
                    <li>Trade Requests (when Trading becomes available)</li>
                </ul>
                </p>

                <p>
                    We need trainers like you to help PokéStop reach it's full potential. If you
                    wish to help
                    contribute to PokéStop's development,
                    <a href="https://github.com/jaewun/pokestop.co.nz">check out our github.</a>
                    If coding isn't your style, that's fine! We invite you to get
                    involved in the forum below.
                </p>
            </div>
        </div>
    </div>

    @can ('createCategories')
        @include ('forum::category.partials.form-create')
    @endcan

    <h2>{{ trans('forum::general.index') }}</h2>

    @foreach ($categories as $category)
        <table class="table table-index">
            <thead>
                <tr>
                    <th>{{ trans_choice('forum::categories.category', 1) }}</th>
                    <th class="col-md-2">{{ trans_choice('forum::threads.thread', 2) }}</th>
                    <th class="col-md-2">{{ trans_choice('forum::posts.post', 2) }}</th>
                    <th class="col-md-2">{{ trans('forum::threads.newest') }}</th>
                    <th class="col-md-2">{{ trans('forum::posts.last') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @include ('forum::category.partials.list', ['titleClass' => 'lead'])
                </tr>
                @if (!$category->children->isEmpty())
                    <tr>
                        <th colspan="5">{{ trans('forum::categories.subcategories') }}</th>
                    </tr>
                    @foreach ($category->children as $subcategory)
                        @include ('forum::category.partials.list', ['category' => $subcategory])
                    @endforeach
                @endif
            </tbody>
        </table>
    @endforeach
@stop
