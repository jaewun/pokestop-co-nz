<tr id="post-{{ $post->sequenceNumber }}" class="{{ $post->trashed() ? 'deleted' : '' }}">
    <td>
        <div class="media ribbon-wrapper">
            <div class="ribbon-wrapper-inner"><div class="ribbon ribbon-{{ strtolower($post->author->team) }}"></div></div>

            <div class="media-left">
                <a href="#">
                    <img src="{{$post->author->pokemon_sprite_url}}" class="media-object" height="64px">
                </a>
            </div>
            <div class="media-body media-middle">
                <h4 class="media-heading">{{$post->authorName}}</h4>
                <div class="text-nowrap"> {{$post->author->name}} </div>
            </div>
        </div>
    </td>
    <td>
        @if (!is_null($post->parent))
            <p>
                <strong>
                    {{ trans('forum::general.response_to', ['item' => $post->parent->authorName]) }}
                    (<a href="{{ Forum::route('post.show', $post->parent) }}">{{ trans('forum::posts.view') }}</a>):
                </strong>
            </p>
            <blockquote>
                {!! str_limit(Forum::render($post->parent->content)) !!}
            </blockquote>
        @endif

        @if ($post->trashed())
            <span class="label label-danger">{{ trans('forum::general.deleted') }}</span>
        @else
            {!! Forum::render($post->content) !!}
        @endif
    </td>
</tr>
<tr>
    <td>
        @if (!$post->trashed())
            @can ('edit', $post)
            <a href="{{ Forum::route('post.edit', $post) }}">{{ trans('forum::general.edit') }}</a>
            @endcan
        @endif
    </td>
    <td class="text-muted">
        {{ trans('forum::general.posted') }} {{ $post->posted }}
        @if ($post->hasBeenUpdated())
            | {{ trans('forum::general.last_updated') }} {{ $post->updated }}
        @endif
        <span class="pull-right">
            <a href="{{ Forum::route('thread.show', $post) }}">#{{ $post->sequenceNumber }}</a>
            @if (!$post->trashed())
                @can ('reply', $post->thread)
                - <a href="{{ Forum::route('post.create', $post) }}">{{ trans('forum::general.reply') }}</a>
                @endcan
            @endif
            @if (Request::fullUrl() != Forum::route('post.show', $post))
                - <a href="{{ Forum::route('post.show', $post) }}">{{ trans('forum::posts.view') }}</a>
            @endif
            @if (isset($thread))
                @can ('deletePosts', $thread)
                @if (!$post->isFirst)
                    <input type="checkbox" name="items[]" value="{{ $post->id }}">
                @endif
                @endcan
            @endif
        </span>
    </td>
</tr>
