<?php

namespace PokeStop;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'pokemon', 'team',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getPokemonSpriteUrlAttribute() {
        return "https://pokeapi.co/media/sprites/pokemon/$this->pokemon.png";
    }
}
