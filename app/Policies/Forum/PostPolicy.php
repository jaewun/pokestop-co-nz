<?php namespace PokeStop\Policies\Forum;

use Illuminate\Support\Facades\Gate;
use Riari\Forum\Models\Post;

class PostPolicy
{
    /**
     * Permission: Edit post.
     *
     * @param  object  $user
     * @param  Post  $post
     * @return bool
     */
    public function edit($user, Post $post)
    {
        return $user->getKey() === $post->author_id ||  $user->role == "admin";
    }

    /**
     * Permission: Delete post.
     *
     * @param  object  $user
     * @param  Post  $post
     * @return bool
     */
    public function delete($user, Post $post)
    {
        return Gate::forUser($user)->allows('deletePosts', $post->thread) || $user->getKey() === $post->user_id;
    }
}
