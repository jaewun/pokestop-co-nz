# PokeStop.co.nz

PokeStop.co.nz is a website aiming to revolutionise trainer relations inside New Zealand.

This is the open source repository for this website. 

## Contributing

We invite you to contribute and improve the website alongside us. Please abide by the Laravel coding standards and don't try to be too fancy. Let's keep it simple.

## Security Vulnerabilities

If you discover a security vulnerability within PokeStop.co.nz, please send an e-mail to Luke Johnson at jaewun+important@me.com. All security vulnerabilities will be promptly addressed.

## License

PokeStop.co.nz is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
